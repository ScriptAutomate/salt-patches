Salt Patches
============

General and CVE related patches for Salt.


List of patches
---------------

[OpenSSL 1.0 Fix](./patches/2016/11/21/0001-Fix-openssl-1.0.patch)

[CVE-2020-11651 and CVE-2020-11652](./patches/2020/04/14/README.md)

[CVE-2020-16846 and CVE-2020-17490](./patches/2020/09/02/README.md)

[CVE-2020-25592](./patches/2020/09/25/README.md)
